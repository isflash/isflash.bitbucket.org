---
layout: master
title: flashdevelop快捷键修改

---


最新版的flashdevelop修改快捷键的地方有变化，以前是在设置选项里面。现在直接放到了Tools目录里面。用习惯了flashbuilder的快捷键，再用flashdevelop的快捷键，明显有点不适应。找到修改的地方后，果断出手修改。下面是我修改快捷键的几个地方。

![](http://amswf.com/images/posts/20120927/1.jpg)

![](http://amswf.com/images/posts/20120927/2.jpg)

![](http://amswf.com/images/posts/20120927/3.jpg)

![](http://amswf.com/images/posts/20120927/4.jpg)

<!-- UY BEGIN -->
<div id="uyan_frame"></div>
<script type="text/javascript" id="UYScript" src="http://v1.uyan.cc/js/iframe.js?UYUserId=1678356" async=""></script>
<!-- UY END -->